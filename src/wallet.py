class Wallet:
    def __init__(self, wallet_id):
        self.wallet_id = wallet_id
        self.balance = 0

    def apply(self, transaction, value):
        if transaction.wallet_id != self.wallet_id:
            return self

        transaction.affect(self, value)
        return self

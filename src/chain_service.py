import requests


class ChainService:
    def __init__(self):
        pass

    def request_node(self, node):
        response = requests.get(f'http://{node}/chain')

        if response.status_code == 200:
            length = response.json()['length']
            chain = response.json()['chain']

            return {
                'chain': chain,
                'length': length
            }
        else:
            return None

import random

from src.transactions import (
    decode as decode_transaction,
    AcceptanceTransaction,
    CashbackTransaction
)
from src.wallet import Wallet


class ValueService:
    CONSENT_COEFS = {
        'A': 0.1,
        'B': 0.1,
        'D': 0.1,
        'E': 0.1,
        'F': 0.1,
        'X': 0.1
    }
    COEFS_TOTAL = sum(value for _, value in CONSENT_COEFS.items())

    def __init__(self, seed):
        self.seed = seed

    def has_wallet(self, blockchain, wallet_id):
        for block in blockchain.chain:
            transaction = block['transaction']

            if transaction['type'] == 'new_wallet' and \
                    transaction['wallet_id'] == wallet_id:
                return True

        return False

    def get_wallet_balance(self, blockchain, wallet_id):
        balance = 0

        for block in blockchain.chain:
            transaction_json = block['transaction']
            transaction = decode_transaction(transaction_json)

            if transaction.wallet_id == wallet_id:
                value = self.assess(transaction)
                balance += value

        return balance

    def assess(self, transaction):
        random.seed(self.seed)
        if isinstance(transaction, AcceptanceTransaction):
            return self.assess_acceptance(transaction.site, transaction.consents)
        elif isinstance(transaction, CashbackTransaction):
            return self.assess_cashback(transaction.amount)
        else:
            return 0.0

    def assess_acceptance(self, site, consents):
        consent_coef = 0.0

        for type, is_granted in consents.items():
            consent_coef += self.CONSENT_COEFS[type] * is_granted
        consent_coef /= self.COEFS_TOTAL

        site_coef = ValueService.assess_site(site)
        return site_coef * consent_coef

    def assess_cashback(self, amount):
        return -amount

    @staticmethod
    def assess_site(site):
        return random.random()

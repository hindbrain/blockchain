import json


class EmptyTransaction:
    def __init__(self, args):
        self.wallet_id = None

    def affect(self, wallet):
        pass


class CashbackTransaction:
    def __init__(self, args):
        self.wallet_id = args['wallet_id']
        self.amount = args['amount']

    def affect(self, wallet, value):
        wallet.balance -= self.amount

    def encode(self):
        return {
            'type': 'cashback',
            'wallet_id': self.wallet_id,
            'amount': self.amount
        }


class AcceptanceTransaction:
    def __init__(self, args):
        self.wallet_id = args['wallet_id']
        self.site = args['site']
        self.consents = args['consents']

    def is_valid(self, blockchain):
        for block in blockchain.chain:
            transaction = block['transaction']

            if transaction['type'] == 'new_wallet' and \
                    transaction['wallet_id'] == self.wallet_id:
                return True

        return False

    def encode(self):
        return {
            'type': 'cookies_acceptance',
            'wallet_id': self.wallet_id,
            'site': self.site,
            'consents': self.consents
        }


class NewWalletTransaction:
    def __init__(self, args):
        self.wallet_id = args['wallet_id']

    def is_valid(self, blockchain):
        for block in blockchain.chain:
            transaction = block['transaction']

            if transaction['type'] == 'new_wallet' and \
                    transaction['wallet_id'] != self.wallet_id:
                return False

        return True

    def affect(self, wallet, value):
        wallet.balance = 0

    def encode(self):
        return {
            'type': 'new_wallet',
            'wallet_id': self.wallet_id
        }


TRANSACTIONS = {
    'cashback': CashbackTransaction,
    'cookies_acceptance': AcceptanceTransaction,
    'new_wallet': NewWalletTransaction
}


def decode(args):
    if args['type'] not in TRANSACTIONS:
        return EmptyTransaction(args)

    ctr = TRANSACTIONS[args['type']]
    return ctr(args)


from uuid import uuid4
from flask import Flask, jsonify, request

from src.value_service import ValueService
from src.blockchain import Blockchain
from src.transactions import (decode as decode_transaction,
                              AcceptanceTransaction, CashbackTransaction, NewWalletTransaction)
from src.wallet import Wallet


app = Flask(__name__)  # Instantiate the Node
blockchain = Blockchain()  # Instantiate the Blockchain
value_service = ValueService(seed=1234)  # Instatntiate the AVE Service

# Generate a globally unique address for this node
node_identifier = str(uuid4()).replace('-', '')


@app.route('/chain', methods=['GET'])
def full_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain),
    }
    return jsonify(response), 200


@app.route('/wallets/<wallet_id>', methods=['GET'])
def wallet(wallet_id):
    wallet = Wallet(wallet_id)
    if value_service.has_wallet(blockchain, wallet_id):
        wallet.balance = value_service.get_wallet_balance(
            blockchain, wallet_id)
        response = {
            'wallet_id': wallet.wallet_id,
            'balance': wallet.balance
        }
        return jsonify(response), 200
    else:
        return jsonify({'error': 'Wallet was not found'}), 404


@app.route('/wallets/new', methods=['POST'])
def new_wallet():
    wallet_id = str(uuid4()).replace('-', '')
    while value_service.has_wallet(blockchain, wallet_id):
        wallet_id = str(uuid4()).replace('-', '')

    transaction = NewWalletTransaction({'wallet_id': wallet_id})
    block = blockchain.new_transaction(transaction)

    response = {
        'message': 'Wallet created',
        'wallet_id': wallet_id
    }
    return jsonify(response), 201


@app.route('/acceptances/new', methods=['POST'])
def new_acceptance():
    args = request.get_json()

    # Check that the required fields are in the POST'ed data
    required_args = ['wallet_id', 'site', 'consents']
    if not all(k in args for k in required_args):
        return 'Missing values', 400

    consent_args = args['consents']
    required_consent_args = ['A', 'B', 'D', 'E', 'F', 'X']
    if not all(k in consent_args for k in required_consent_args):
        return 'Missing values', 400

    if not value_service.has_wallet(blockchain, args['wallet_id']):
        return jsonify({'message': 'Wallet was not found'}), 404

    # Create a new Transaction
    transaction = AcceptanceTransaction(args)
    block = blockchain.new_transaction(transaction)

    response = {
        'message': f'Site cookie policy acceptance registered', 'block': block}
    return jsonify(response), 201


@app.route('/cashbacks/new', methods=['POST'])
def new_cashback():
    args = request.get_json()

    required = ['wallet_id', 'amount']
    if not all(k in args for k in required):
        return jsonify({'message': 'Missing values'}), 400

    if not value_service.has_wallet(blockchain, args['wallet_id']):
        return jsonify({'message': 'Wallet was not found'}), 404

    wallet_balance = value_service.get_wallet_balance(
        blockchain, args['wallet_id'])
    if args['amount'] > wallet_balance:
        return jsonify({'message': 'You have not enough credit to perform the cashback'}), 400

    transaction = CashbackTransaction(args)
    block = blockchain.new_transaction(transaction)

    response = {
        'message': f'Cashback transaction has been ordered', 'block': block}
    return jsonify(response), 201


@ app.route('/nodes/register', methods=['POST'])
def register_nodes():
    values = request.get_json()

    nodes = values.get('nodes')
    if nodes is None:
        return "Error: Please supply a valid list of nodes", 400

    for node in nodes:
        blockchain.register_node(node)

    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(blockchain.nodes),
    }
    return jsonify(response), 201


@ app.route('/nodes/resolve', methods=['GET'])
def consensus():
    replaced = blockchain.resolve_conflicts()

    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': blockchain.chain
        }
    else:
        response = {
            'message': 'Our chain is authoritative',
            'chain': blockchain.chain
        }

    return jsonify(response), 200


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000,
                        type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host='0.0.0.0', port=port)
